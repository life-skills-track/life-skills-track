# Focus Management

### What is Deep work

* Deep work is characterized by a person's capacity to concentrate intensely on complex tasks for a significant amount of time, without any disturbances. This implies being fully absorbed in the task at hand, with an unwavering focus and minimal interruptions.
*  Deep work also requires sustained focus, mental effort, and a clear sense of purpose. It also includes working on complex problems or projects that require significant mental effort and attention such as writing, coding, research or creative work.


### Summary of Deep Work Book

* The optimal duration of deep work can vary depending on the person and the task, and research says that most people can do deep work for 1 hour to 90 minutes at a time, before taking a break. This is because our brain needs time to rest and recharge after long periods of intense concentration. Therefore, it's necessary to take short breaks between deep work sessions to allow for recovery and to maintain productivity and focus.
* Deadlines are good for productivity because they set clear expectations and a sense of urgent work which can motivate a person to work more efficiently and effectively.
* A person has to recognize the deep work and make it a priority of their work life.
* Deep work requires undivided attention. So a person has to develop a deep work routine and minimize distractions during those periods. Developing simple and regular habits to generate a rhythm for the work.
* Evening shutdown, to do deep work sleep is a very important thing a person has to do. A good deep sleep will allow you to get more focus on deep work.

### How can you implement the principles in your day-to-day life?

I can implement the principles in my day-to-day life by doing the following things.
* Schedule the time of the day for work when I can work without any interruptions. or distractions.
* Remove possible distractions during the deep work. such as mobile phone notifications, social media or anything else.
* Set a clear goal for what I want to achieve in the deep work session.

# Dangers of Social Media

* Social media are highly addictive. Excessive use of social media can affect other areas of our life such as work, relationships, and physical and mental health.
* People who don't use social media are happier and more sustainable in their life.
* Social media is just a source of entertainment, and it takes your time and bites your data which is sold later.
* Assumptions like If I do not have a social media brand people won't know who I am, people won't able to find me and opportunities won't come my way are complete nonsense.
* Social media affect an individual's ability to focus. It can permanently reduce the capacity of concentration. you can permanently reduce your capacity to do a deep effort towards work.
* It can also raise anxiety related disorders.




