# Tiny Habits

* Tiny Habits are small and easy-to-implement actions that can have a significant impact on our lives over time. By starting with small changes, we can create long-lasting habits that can lead to positive outcomes.

* To make tiny habits a part of our routine, we need to do them consistently. It's better to choose a small habit that we can realistically stick to, rather than trying to do too much all at once, which might be too hard.

* Tiny habits can be used to achieve a wide variety of goals, from improving physical health and well-being to improving productivity and creativity. By identifying the specific outcomes you want to achieve, you can choose tiny habits that will help you to achieve your goal.

* Over time, consistently repeating small behaviours has a positive impact. By doing these behaviours regularly, we train our brain to reinforce them and eventually they become automatic.

* Tiny habits are small actions we can take to improve our lives, such as drinking water when we wake up, stretching or exercising for a few minutes each day, taking breaks throughout the workday, setting daily goals, and being thankful for three things each day. It's important to choose habits that fit our goals and that we can realistically include in our daily routine.




## Core Message: 

1. Shrink the Behaviour: If a task is too hard then you need to have a high level of motivation to complete the task. If a task is easy to do you need very little motivation to complete the task. Shrinking every new habit in the tiniest possible version so that very little motivation is needed to do it. 
2. Identify an Action Prompt: There are three types of prompts: External prompts, internal prompts and Action Prompts. 
Most people rely on External and internal prompt which as phone notification, and internal thoughts which is too easy to ignore that's why it's fails most of the time. because you need to get out of your comfort zone to do the task.
Where the Action prompts use the completion of one behaviour to trigger the new behaviour. It goes like this "After I______ I will______".
3. Grow habits with some shine: Developing a habit is like growing a small tree. Shine is the term to explain the feeling you get after an accomplishment(Authentic pride).
Learn to celebrate after each tiny habit. It may sound ridiculous to celebrate after a tiny habit but learning the celebrate after a tiny win is the most critical component of habit development.
When you feel successful at something even if it is tiny your confidence grows very quickly and motivation increases to do that habit again.

* By making small, easy-to-do changes to our behaviour, we can create long-lasting habits that have a positive impact on our lives.
The importance of starting small and focusing on consistency rather than relying on willpower to make big changes. 
By adopting tiny habits that align with our goals and values, we can create positive feedback loops that reinforce our behaviour and lead to lasting change. 

#### How can you use B = MAP to make making new habits easier?

By using the B = MAP model, I can identify the factors that make it easier or harder to adopt a new habit and develop strategies to increase motivation, ability, and prompts.
* To increase motivation, I can make the behaviour more appealing by focusing on the benefits or positive outcomes that come with it.
* To increase ability, I can break the behaviour down into smaller, more manageable steps or make it easier to perform by removing distractions.
* To increase prompts, I can create reminders that make it easier to remember to perform the behaviour one after another.


#### Why it is important to "Shine" or Celebrate after each successful completion of a habit?

It's important to celebrate after completing a habit because it helps to reinforce the behaviour and make it more likely that we will stick to it in the future. When we celebrate, whether it's with a small reward or simply by acknowledging our accomplishments, we feel good and proud of ourselves. This positive feeling strengthens our confidence and motivation, which makes it easier to repeat the behaviour in the future.


# 1% Better Every Day Video

* Focusing on making small, incremental improvements over time. By doing so, you can achieve significant progress and growth.
* Identify areas of your life where you want to improve and then set small, achievable goals that align with those areas.
* It's important to stick to your goals and routines even when you don't feel like it or when progress seems slow.
* Celebrating your progress along the way is important when working towards getting 1% better every day.
* It is achievable and sustainable because it requires minimal effort and is easy to integrate into our daily lives.


# Book Summary of Atomic Habits

#### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes.
To build new habits that last, we need to focus on three things: who we want to be, what we do every day, and what we want to achieve. This means that we should try to identify with the kind of person who embodies the habits we want to have, take small and consistent actions towards our goals, and stay focused on the good things that will come from adopting new habits. By doing these things, we can create positive change in our lives.

#### Write about the book's perspective on how to make a good habit easier.
To make a good habit easier to adopt and stick with, we need to create an environment that supports us. This means making it easy and fun to do the habit and creating positive experiences around it. We can do this by making the habit obvious, attractive, easy, and satisfying. By doing these things, we can create a positive cycle that reinforces the habit and makes it more likely that we will continue to do it over time.

#### Write about the book's perspective on making a bad habit more difficult.
To break a bad habit, make it more difficult to do. This is because when a bad habit is easy to do, we are more likely to give in to the temptation to do it. By making a bad habit more difficult to do, we create barriers that make it less likely that we will engage in the habit. Over time, this can help us break the habit and replace it with a more positive one.

# Reflection:

#### Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
Reading books. Here are some steps I can take:
* Making it obvious: By placing my book somewhere visible.
* Making it more attractive: By Choosing a book that aligns with my interests or a topic.
* Making it easier: Breaking down the reading time for about 10-15 minutes a day.


#### Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
Using Social media. Here are some steps I can take. 
* Making the cue invisible: By removing social media apps from the phone's home screen. Turn off notifications or set specific times to check social media.
* Making the process unattractive: By unfollowing negative accounts or limiting the amount of time spent on social media.
* Making the response harder: Finding ways to make it more difficult to access social media, such as by setting a password or using two-factor authentication to log in or disabling automatic logins.