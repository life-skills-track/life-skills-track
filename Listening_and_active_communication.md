# Listening and active communication

## Active listening 

### Steps/Strategies to do active listening.
* Active listening is an act of fully hearing and understanding the meaning of what someone is saying.
* Avoid your thoughts while communicating and listen to the person carefully. Make sure to focus on the speaker and the topic.
* Try not to interrupt the other person while communicating, let them finish first and then respond.
* While communicating you can use phrases like, "I am listening", "That Sounds Interesting" etc. This will show that you are interested.
* You can also use your body language to show that you are listening.
* During an important conversation you can also take notes if appropriate. 
* Paraphrase what the other person has said to make sure that both people are on the same topic.
 
## Reflective listening

### Key points of Reflective listening.
* Reflective listening is a communication strategy to find the idea of the speaker while communicating and then offer back the idea to make sure that both people are on the same track. 
* Listener should understand the needs of the person speaking. If the listener is not able to understand it may lead to problems such as errors in work, problems remaining unresolved, etc.

## Reflection

### What are the obstacles in your listening process?
* The obstacle in my listening process is my thoughts, when a communication goes long or it seems boring, various thoughts came into my mind. 

### What can you do to improve your listening?
* I can try not to get lost in my thought during communication. I can also use key points of reflective listening. It will also improve my listening.

### When do you switch to Passive communication style in your day to day life?
* I switch to passive communication when someone asks me to do something, I know that I am capable of doing that work and they are not. Even if I don't want to work at that point in time I agree to do that to make them feel good.

### When do you switch into Aggressive communication styles in your day to day life?
* When I am sure about things and try to make people understand and they are ignoring me. Then I switch to an aggressive communication style.
* Mostly I avoid switching to Aggressive communication, as it can damage relationships. 
* While dealing with a dangerous situation or threatening situation I switch to Aggressive communication. 

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
* When someone gives me advice without having any knowledge of a particular field. Then I switch to Passive Aggressive communication.
* When someone gets personal while gossiping and making fun of each other, then I switch to passive Aggressive communication.

### How can you make your communication assertive? what steps you can apply in your own life?
* Assertive communication is about expressing your needs and opinions while also respecting others.
* I will ask about my needs clearly and directly. Also making respect for other person feelings.
* I will set my boundaries where what I can tolerate and what not.








