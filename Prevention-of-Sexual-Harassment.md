# Prevention of Sexual Harassment

#### What kinds of behaviour cause sexual harassment?
* Sexual harassment can take many forms, and it's important to note that it's always defined by the person experiencing it. 
* Given below are the kinds which cause sexual harassment:
1. Passing comments or jokes about a person's body's appearance.
2. Physical touching such as unwanted hugs, kisses, rubbing and other forms of unwanted touch or physical contact.
3. Displaying or sharing sexually explicit images, posters or materials.
4. Stalking a person using social media, text messages etc.
5. Using the power of position to make pressure on someone into sexual acts.
6. When a person makes threats or promises to offer rewards or punishments in exchange for sexual activity.

#### What would you do in case you face or witness any incident or repeated incidents of such behaviour?
* In case I face or witness any incident or repeated incidents of such behaviour I would do the following things:
1. Speak to the person who is engaging in the behaviour and let them know that it is not acceptable.
2. Collecting the evidence about what happened, when happened and who does it. 
3. If this thing continues then report to the human resources department or another person in authority.
4. If feeling unsafe or threatened then contact the police. 