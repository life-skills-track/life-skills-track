# Energy Management

#### What are the activities you do that make you relax - Calm quadrant?
* Activities that I do to make me relax - Calm quadrant are : 
1. Going for a walk, walking in the early morning and evening in parks, gardens or fields where I can see nature, and feel the fresh air.
2. Listening to classical music, this thing keeps me calm.
3. Spending time with my friends and family members.


#### When do you find getting into the Stress quadrant?
* I generally find myself getting into the stress quadrant when:
1. Facing new work-related challenges when I am unaware of my ability.
2. Facing financial challenges.
3. Negative thoughts come to my mind.

#### How do you understand if you are in the Excitement quadrant?

* I understand that I am in the Excitement quadrant when: 
1. I am confident about my ability to perform well while facing challenges.
2. I do activities related to my hobbies such as cooking, playing etc.



# Sleep is your superpower

* Sleep is the most important part of our life. It helps us to boost our brains to work more effectively and learn new things properly.
* It is discovered that you need to sleep after learning that way our brain makes the memory about what we've learnt so far. you also need to sleep before learning so that your brain is empty of nonsense information and ready to absorb the new memory.
* It is also discovered that those people who have not slept well will have 40% less learning ability in their brain compared to the person who has slept well.
* As we get older our learning ability gets reduced. that has not happened because a person is ageing but is not taking good quality sleep.
* By taking a good quality sleep we can bring back our healthy quality of brain learning activity.
* Reducing the time of sleep will also increase the risk of a heart attack. And getting a good 8 hours of deep sleep will decrease the risk of heart attack by 21%.
* Sleeping also helps our immune system by producing natural killer cells these are proteins that help fight off infections and inflammation.
* Reducing sleep can also lead to serious health issues like cancer etc.
To improve our sleep quality we have to regularize our schedule by going to sleep at the same time and waking up at the same time. Sleep is our life support system it should not be negotiated.


#### What are some ideas that you can implement to sleep better?

ideas that I can implement to sleep better are:
1. Sticking to a regular sleep schedule.
2. Create a comfortable sleep environment. 
3. Avoid using electronic devices before going to bed.
4. Doing regular exercise. 

# Brain-Changing Benefits of Exercise

* Exercising regularly will improve your brain activities such as learning, Getting new ideas, focusing and maintaining attention for a longer time.
* Exercising has an immediate effect on your brain. working out will increase the levels of neurotransmitters like Dopamine etc. that going to result in a better mood, better Energy, better memory, and better attention, right after that workout. It also improves your reaction time for a longer time.
* when we exercise the hippocampus of the brain will produce new brain cells that increase its volume and helps in long-term memory.
* Doing Excercise for at least 30 minutes most days of the week will benefit our brain health.

#### What are some steps you can take to exercise more?
* Steps I can take to exercise more.
1. I can set a specific goal to exercise for a certain amount of time. 
2. Finding an exercise which I can enjoy the most whether it is running, cycling, or any other activity. 
3. Making a habit of exercise setting time for the exercise and sticking to the regular schedule.
