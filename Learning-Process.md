# Learning Process

### What is Feynman Technique?
* The Feynman Technique is a learning and Problem-solving technique. By Breaking down bigger and more complex problems into small and simple components. Explaining the problem in such a way, that a complete beginner can easily understand.
* Here are some steps to follow Feynman Technique :
    * First of all select a topic which you want to learn and note it down on a paper.
    * Explain the topic in simple language which is easy to understand. As you are teaching a beginner.
    * Also use the examples which are using the concepts. 
    * Make sure to identify the problem area which is halting your explanation, go back to the resources, go through examples and study more in detail. 
    * Make your explanation as simple as possible. you can repeat the process until you can to explain it to someone without taking help from notes.

### What are the different ways to implement this technique in your learning process?
* There are various ways to implement this technique in the learning process. 
* You can visualize the problem and draw a flowchart or diagram on paper or whiteboard maybe. Doing this will help you see the flow of the problem.
* You can explain the problem to someone such as your friend or family member as you are teaching them and take feedback from them. Because if you can explain it to someone then you must have understood the problem.

## Learning How to Learn
* Our brain is very complex. It normally has two different modes, the first one is the focused mode,so when you turn your attention towards any learning process, it gets on. The second one is the relaxed mode.
So when you are learning something your brain is going into these two modes.
* When you are learning, your thought process is going to some historical patterns that your brain has already gone through. That's how focused mode works.
* If there is something new to learn then you have to think of a different way or you have the see the problem from a different perspective. Here you have to use another mode of your brain, to get some new ideas.
* If you are trying to learn a problem and you found out that you are stuck somewhere then turn your attention from the focused mode to the relaxed mode and let them do their work in the background.
* Implement these techniques in real life by using timers. Set the timer for 25 minutes then you work in focused mode and after 25 minutes you do some fun activities. By doing this you can relax and use the relaxed mode of your mind. And for 25 minutes you will be focused towards work.
* Even if you are a slow learner, Exercise and Give tests and make sure that you understand the deep concepts and have mastery of the concept. 


### What are some of the steps that you can take to improve your learning process?
Here are some steps to improve your learning process.
* Breakdown any problem into smaller components.
* Take breaks while learning. Relaxing is very important to improve your learning process. You can set your timer.
* Make sure their is no distraction when you are in learning process. 

## Learn anything in 20 hours.
* Learning is the process of acquiring new knowledge and skills that you might not have already.
* Learning a new skill takes time. but a little bit of practice will make you better and better.
* At starting you will acquire things really quickly as you are giving time for practice but this process will slow down when you reduce your time.
* The Research says that if you want to learn something new, then give at least 20 hours. By giving about 45 minutes without any distractions for a month. It will make you better and better.
* Most of the skills are a Bundle of various skills so choose a skill and break it down into smaller pieces. And you can practice those first. 
* Make sure to remove the barriers while learning such as the internet, phone calls etc. these things are making you sit down. 
* Learning something new will make you feel stupid at first that is because you do not have those skills, remove that emotional barrier from your head and practice. 

### What are some of the steps that you can while approaching a new topic?
* While approaching a new topic remove the emotional barriers which are stopping me from learning. These thoughts will surely come into my mind in the beginning.
* Breaking the problem into smaller pieces. And make sure to understand each topic and have a deep understanding of the topic.
* Practice every single day. Give time to that skill. Learning is a process of acquiring skills and knowledge. And by practising it every day will make you an expert in a particular field.