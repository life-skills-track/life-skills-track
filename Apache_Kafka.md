# Apache Kafka

While dealing with a huge amount of information. It is very difficult to gather a large volume of data, and it is very hard to analyze and understand it. To handle such kinds of situations we need a messaging system. And here comes Apache Kafka.
* Apache Kafka is an open-source distributed open-source messaging platform designed to handle large-scale data streams and high throughput systems.
* Kafka was created as a solution for real-time data processing, especially when dealing with high volumes of data. It is designed to be fast, scalable and reliable.

## Characteristics of Kafka
* One of the key benefits of Kafka is its ability to handle a high volume of data with low latency.
* It has been tested to handle millions of events per second which makes it a good fit for real-time data processing applications.
* It has built-in partitioning and replication features which ensure that data is always available even in any case of failure.
* Any Data written to a topic is stored on a disk and their multiple copies and kept across various brokers which ensures the data's availability in case of failure.
* Kafka is also highly flexible, and it can also be integrated with various other technologies and systems. For example, it can be used in conjunction with Apache Spark for real-time data analysis, or with Apache Cassandra for data storage.

## How Kafka works
* When an event (an event will have a key value and timestamp and may also contain optional metadata and headers) occurs like a website visit to the producer, API creates a new record these records are stored on disks in an ordered immutable log called a topic (a topic is just a log of the event kept in order), which can persist forever or disappear when no longer needed. Topics are distributed and replicated in a cluster which contains multiple servers called Brokers. 
* This makes Kafka fully tolerant and able to scale to any workload. On the other side, multiple users can subscribe to this Data, they can read the most recent message like a queue or read the entire topic log and listen to updates in real-time, in addition, it provides very powerful streams.
* It is guaranteed that any consumer of a given topic will always read the event in the same order.
* It is used today by companies like Spotify, and Netflix for log processing, and Cloudflare for real-time analytics. Its benefits make it a valuable asset for any organization looking to process and analyze real-time data.

## Challenges in Kafka
* One of the key challenges while using Kafka is managing the complexity of the system.
* When the number of topics and consumer grows, it can become difficult to manage and monitor the system effectively.

Despite these challenges, the benefits of Kafka make it a popular choice for a variety of use cases. Its high performance, scalability, and reliability make it well-suited for real-time data processing applications, and its publish-subscribe model allows for easy integration with a variety of other technologies.


### References
* https://youtu.be/uvb00oaa3k8
* https://www.tutorialspoint.com/apache_kafka/index.htm
* https://www.confluent.io/what-is-apache-kafka/
