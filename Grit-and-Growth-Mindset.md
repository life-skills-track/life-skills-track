# Grit 

* Grit is a psychological trait that refers to an individual's passion for long-term goals. It is the ability to stick to their interest and making effort for a long-term goal, even in hard conditions.

* We can see many people joining different organizations. But people having a high level of Grit will survive and put effort to achieve their goals.

* It does not mean that only people having high IQ or talent will survive and achieve their goals. Achieving the goal will require stamina, discipline and very importantly not giving up.

### What are your key takeaways from the video to take action on?

* Be consistent about what you are doing.
* Discipline is the key to success. Make sure to be in stick with the future. 
* Working hard to make that future in reality.
* Ability to learn is not fixed it can change if you put effort.

# Growth Mindset

* There are two types of Mindset first one growth mindset and second one is fixed mindset.

* People with Growth Mindset believe that their abilities are not fixed and they always try to learn new things they also believe that their abilities and qualities will grow over time. These people also take challenges and risks in life without fear of failure they see it as a chance to learn new things.
* People with a Fixed Mindset believe that their abilities are fixed which they are born with and these abilities cannot be changed or grown. These people can easily give up.

### What are your key takeaways from the video to take action on?
* Making an Effort for doing something. Even if it may not necessary or not useful, it will lead to growth.
* Facing challenges and learning new things. See challenges as an opportunity to learn and get better.
* Doing mistakes and also the part of learning new things.
* Receiving feedback from anyone without taking it personally and working on them.

# Understanding Internal Locus of Control

* Internal Locus of control is a psychological concept that refers to a person's belief about which they have control over the events that affect their lives.

* People having Internal Locus of control believe that they have control over their own life. They appreciate and admire their work. These people are self-motivated, take responsibility and appreciate their work, they do not believe in luck and do not blame others for their failures.

* Solving problems of our own lives, taking some time and appreciating the actions taken.

# How to build a Growth Mindset

Building a Growth mindset leads to greater achievement, personal growth, and overall happiness. Believing in yourself and making efforts to turn your dreams into reality.

### What are your key takeaways from the video to take action on?

* Believe in your ability to figure things out. Believe that I can get better.

* Don't assume that you are not capable enough to do that.

* Make a curriculum of your own life. Don't wait for someone to give you 
all the support. Think of your dreams and architect your curriculum to achieve that dreams.

* Sometimes life becomes more challenging and struggle is part of your learning journey. It seems difficult but that difficulty will give you strength and ability to handle more pressures and make you tough.


# Mindset - A MountBlue Warrior Reference Manual

* I will understand each concept properly.
* I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.


